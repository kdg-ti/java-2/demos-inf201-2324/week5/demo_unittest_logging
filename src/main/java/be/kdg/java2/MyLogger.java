package be.kdg.java2;

public class MyLogger {
    public static boolean ON = true;

    public static void log(String message) {
        if (ON) System.out.println(message);
    }
}
