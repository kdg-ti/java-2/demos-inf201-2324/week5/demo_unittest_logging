package be.kdg.java2;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.LogManager;

public class Main {
    public static void main(String[] args) {
        URL logfileURL = Main.class.getResource("/mylogging2.properties");
        try (InputStream logfileis = logfileURL.openStream()) {
            LogManager.getLogManager().readConfiguration(logfileis);
        } catch (IOException e) {
            System.err.println("problem opening log properties!");
        }
        //MyLogger.ON = false;
        Student student = new Student();
        student.studeer();
        System.out.println(student);
    }
}
