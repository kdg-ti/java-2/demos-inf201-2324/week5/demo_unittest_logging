package be.kdg.java2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RekenmachineTest {
    private Rekenmachine rekenmachine;

    @BeforeEach
    public void init(){
        rekenmachine = new Rekenmachine();
    }

    @Test
    public void testSom() {
        int resultaat = rekenmachine.som(10, 20);
        assertEquals(30, resultaat, "Er zit een fout in de som methode!");
    }

    @Test
    public void testMaal() {
        double resultaat = rekenmachine.maal(10, 20);
        assertEquals(200, resultaat, "Er zit een fout in de maal methode!");
    }

    @Test
    public void testDeling() {
        double resultaat = rekenmachine.deling(20, 10);
        assertEquals(2, resultaat, "Er zit een fout in de deling methode!");
    }

    @Test
    public void testSetNameThrowsException(){
        assertThrows(IllegalArgumentException.class, ()->rekenmachine.setName(null),
                "setName zou IllegalArgumentException moeten throwen als de name null is!");
    }
}
