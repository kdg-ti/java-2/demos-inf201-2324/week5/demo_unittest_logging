package be.kdg.java2;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Student {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private String name;
    public Student() {
        //MyLogger.log("constructor van Student aangeroepen...");
        logger.info( "constructor van Student aangeroepen...");
        this.name = "anoniem";
    }

    public void studeer(){
        logger.info( "start van studeer methode");
        System.out.println("Ik studeer...");
    }

    @Override
    public String toString() {
        logger.fine("toString methode van Student aangeroepen");
        return name;
    }
}
