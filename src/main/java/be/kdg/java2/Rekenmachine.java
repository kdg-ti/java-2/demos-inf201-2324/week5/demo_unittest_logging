package be.kdg.java2;

public class Rekenmachine {
    private String name;

    public void setName(String name){
        if (name==null||name.isEmpty()) {
            throw new IllegalArgumentException("Naam mag niet leeg zijn!");
        }
        this.name = name;
    }

    public int som(int a, int b) {
        return a - b;
    }

    public double deling(double a, double b) {
        return a / b;
    }

    public double maal(double a, double b) {
        return a * b;
    }
}
